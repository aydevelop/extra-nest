import {
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class CategoryDTO {
  id: number;
  name: string;
}

export class CreateCategoryDTO {
  categories: CategoryDTO[];
}
