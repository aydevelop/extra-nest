import { Body, Controller, Post } from '@nestjs/common';
import { CreateCategoryDTO } from './dto/create-category.dto';
import { CreateQuestionDTO } from './dto/create-question.dto';
import { QuizService } from './quiz.service';

@Controller('quiz')
export class QuizController {
  constructor(private quizService: QuizService) {}

  @Post('categories')
  async createCategories(
    @Body() categories: CreateCategoryDTO,
  ): Promise<any[]> {
    return await this.quizService.createCategories(categories);
  }

  @Post('questions')
  async createQuestions(@Body() questions: CreateQuestionDTO): Promise<any[]> {
    return await this.quizService.createQuestion(questions);
  }
}
