import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateProductDTO } from 'src/products/dtos/create-product.dto';
import { Repository } from 'typeorm/repository/Repository';
import { CategoryEntity } from './category.entity';
import { CreateCategoryDTO } from './dto/create-category.dto';
import { CreateQuestionDTO } from './dto/create-question.dto';
import { Category } from './models/category.interface';
import { QuestionEntity } from './question.entity';

@Injectable()
export class QuizService {
  constructor(
    @InjectRepository(QuestionEntity)
    private readonly questionRepository: Repository<QuestionEntity>,

    @InjectRepository(CategoryEntity)
    private readonly categoryRepository: Repository<CategoryEntity>,
  ) {}

  async createCategories(category: CreateCategoryDTO): Promise<Category[]> {
    return await this.categoryRepository.save(category.categories);
  }

  async createQuestion(question: CreateQuestionDTO): Promise<any> {
    return await this.questionRepository.save(question);
  }
}
