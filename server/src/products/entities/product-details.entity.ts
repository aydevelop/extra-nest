import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'products-details' })
export class ProductDetailsEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  partNumber: string;

  @Column({ nullable: true })
  dimension: number;

  @Column({ nullable: true, type: 'float' })
  weight: number;

  // @Column({ length: 45 })
  @Column({ nullable: true })
  manufacturer: string;

  // @Column({ length: 45 })
  @Column({ nullable: true })
  origin: string;
}
