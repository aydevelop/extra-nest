import {
  Get,
  Post,
  Param,
  Body,
  Delete,
  HttpException,
  HttpStatus,
  ForbiddenException,
  NotFoundException,
  Req,
  Put,
} from '@nestjs/common';
import { Controller } from '@nestjs/common';
import { CreateProductDTO } from './dtos/create-product.dto';
import { Product } from './interfaces/product.interface';
import { ProductsService } from './products.service';
import { Request } from 'express';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { ProductsEntity } from './entities/products.entity';
import { UpdateProductDTO } from './dtos/update-product.dto';

@Controller('products')
// @UseFilters(HttpExceptionFilter)
// @UseInterceptors....
export class ProductsController {
  constructor(private productService: ProductsService) {}

  @Post()
  async create(@Body() product: CreateProductDTO) {
    return await this.productService.create(product);
  }

  @Get()
  async find(): Promise<Product[]> {
    console.log('findAl2l');
    return await this.productService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id): Promise<Product> {
    return await this.productService.findOne(id);
  }

  // @Delete(':id')
  // async delete(@Param('id') id): Promise<DeleteResult> {
  //   return await this.productService.delete(id);
  // }

  //@Delete(':id/:detailsId')
  @Delete(':id')
  async delete(@Param() params): Promise<DeleteResult> {
    return await this.productService.delete(params.id);
    console.log('id ' + params.id);
    console.log('detailsId ' + params.detailsId);
    return null;

    // await Promise.all(
    //   await this.productRepository.delete(params.id),
    //   await this.productDetailsRepository.delete(params.detailsId);
    // );

    return await this.productService.delete(params.id);
  }

  @Put(':id')
  async update(
    @Param('id') id,
    @Body() recordToUpdate: UpdateProductDTO,
  ): Promise<UpdateResult> {
    return await this.productService.update(id, recordToUpdate);
  }

  // @Post()
  // async create(@Body() product: CreateProductDTO): Promise<Product> {
  //   console.log('test');
  //   console.log('test');
  //   console.log('test');
  //   console.log('test');
  //   console.log('test');
  //   return this.productService.create(product);
  // }

  // @Get()
  // async find(@Req() request: Request): Promise<Product[]> {
  //   //console.log(request['user']);
  //   return this.productService.findAll();
  // }

  // @Get()
  // async find(@Req() request: Request) {
  //   return 'some text';
  //   //console.log(request['user']);
  //   //return this.productService.findAll();
  // }

  // @Get(':id')
  // async findOne(@Param() params): Promise<Product> {
  //   return this.productService.findOne(params.id);
  // }

  // @Delete(':id')
  // async delete(@Param() params): Promise<Product[]> {
  //   //return this.productService.delete(params.id);

  //   throw new NotFoundException();
  //   throw new ForbiddenException();
  //   throw new HttpException('something went wrong', HttpStatus.BAD_REQUEST);
  // }
}
