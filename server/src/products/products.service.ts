import { Injectable, NotFoundException } from '@nestjs/common';
import { Product } from './interfaces/product.interface';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { ProductsEntity } from './entities/products.entity';
import { CreateProductDTO } from './dtos/create-product.dto';
import { UpdateProductDTO } from './dtos/update-product.dto';
import { ProductDetailsEntity } from './entities/product-details.entity';

@Injectable()
export class ProductsService {
  // products: Product[] = [{ id: '1', name: 'test_name', qty: 10, price: 100 }];

  constructor(
    @InjectRepository(ProductsEntity)
    private readonly productRepository: Repository<ProductsEntity>,

    @InjectRepository(ProductDetailsEntity)
    private readonly productDetailsRepository: Repository<ProductDetailsEntity>,
  ) {}

  async create(product: CreateProductDTO): Promise<Product> {
    // const productDetails = await this.productDetailsRepository.save({
    //   partNumber: product.partNumber,
    // });

    const newProduct = new ProductsEntity();
    newProduct.name = product.name;
    newProduct.price = product.price;
    newProduct.qty = product.qty;
    //newProduct.productDetails = productDetails;

    return await this.productRepository.save(newProduct);
  }

  async findAll(): Promise<Product[]> {
    return await this.productRepository.find({ relations: ['productDetails'] });
  }

  async findOne(id: number): Promise<Product> {
    const results = await this.productRepository.findOne(id, {
      relations: ['productDetails'],
    });
    if (!results) {
      throw new NotFoundException('Could not find any product');
    }

    return results;
  }

  //async delete(id: string): Promise<DeleteResult> {
  async delete(id: string): Promise<any> {
    await Promise.all([
      // await this.productDetailsRepository.delete(id);
      await this.productRepository.delete(id),
    ]);
  }

  async update(
    id: number,
    recordToUpdate: UpdateProductDTO,
  ): Promise<UpdateResult> {
    // const product = await this.productRepository.findOne(id, {
    //   relations: ['productDetails'];
    // })
    //if(!product) {trhow NotFoundException("msg")}
    //const { qty, price, name } = recordToUpdate;
    // await this.rpductRepository.merge(product, {
    //   qty,
    //   name,
    //   price
    // })
    // const updateProdu = await this.productRepostiory.save(product);
    // const foundDetails = await this.productDetailsRepository.findOne(prod.details.id);
    // const { dimension, weight, origin, manufacturee } = recTOUpd;
    // await this.productDetailsRepository.merge(foundDetails, {
    //   qty,
    //   name,
    //   price
    // });
    // const upatedDetails = await this.productDetailsRepository.save(foundDetails);
    // return { ...updatedProduct, productDetails: updatedProducts };

    Object.keys(recordToUpdate).forEach((key) =>
      !recordToUpdate[key] ? delete recordToUpdate[key] : {},
    );

    return await this.productRepository.update(id, recordToUpdate);
  }

  // findAll(): Product[] {
  //   return this.products;
  // }

  // findOne(id: string): Product {
  //   //return this.products.find((p) => p.id === id);
  //   return null;
  // }

  // delete(id: string): Product[] {
  //   // const index = this.products.findIndex((p) => p.id === id);
  //   // if (index >= 0) {
  //   //   this.products.splice(index, 1);
  //   // }

  //   // return this.products;

  //   return null;
  // }
}
