import { ProductDetailsEntity } from '../entities/product-details.entity';

export interface Product {
  id: number;
  name: string;
  qty: number;
  price: number;
  productDetails?: ProductDetailsEntity;
}

export interface productDetails {
  id?: string;
  partNumber?: string;
  dimension?: string;
  weight?: number;
  manufacturer?: string;
  origin?: string;
}

export interface UpdateProduct {
  name: string;
  qty: number;
  price: number;
}
