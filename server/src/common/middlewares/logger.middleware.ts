import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  use(req: any, res: any, next: () => void) {
    //console.log('LoggerMiddleware run ...');
    req.user = 'admin user';
    //res.status(200).json({ test: 123 });
    next();
  }
}
