import { PhotosEntity } from 'src/photos/photo.entity';
import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'users' })
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  password: string;

  @OneToMany(() => PhotosEntity, (photo) => photo.user, {
    cascade: ['insert', 'update'],
    onDelete: 'CASCADE',
  })
  photos: PhotosEntity[];
}
