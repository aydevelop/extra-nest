import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { jwtConstants } from 'src/constants';
import { PhotosEntity } from 'src/photos/photo.entity';
import { JwtStrategyService } from './auth/jwt-strategy.service';
import { PasswordHasherService } from './auth/password-hasher.service';
import { UserEntity } from './entities/users.entity';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

@Module({
  imports: [
    JwtModule.register({ secret: jwtConstants.secret }),
    TypeOrmModule.forFeature([UserEntity, PhotosEntity]),
  ],
  controllers: [UsersController],
  providers: [UsersService, PasswordHasherService, JwtStrategyService],
})
export class UsersModule {}
