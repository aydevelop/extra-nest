import { RolesGuard } from './../common/guards/role-guard';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Request,
  SetMetadata,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { DeleteResult, UpdateResult } from 'typeorm';
import { CreateUserDTO } from './dto/create-users.dto';
import { UpdateUserDTO } from './dto/update-users.dto';
import { User } from './interfaces/user';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private userService: UsersService) {}

  @Get('profile')
  @UseGuards(AuthGuard('jwt'))
  // @UseGuards(RolesGuard)
  @SetMetadata('roles', ['admin'])
  async profile(@Request() req) {
    return req.user;
  }

  @Post()
  async create(@Body() user: CreateUserDTO): Promise<User> {
    return await this.userService.create(user);
  }

  @Get()
  async findAll(): Promise<User[]> {
    return this.userService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id', ParseIntPipe) id): Promise<User> {
    return await this.userService.findOne(id);
    //return await this.userService.
  }

  @Delete(':id')
  async delete(@Param('id', ParseIntPipe) id): Promise<DeleteResult> {
    return await this.userService.delete(id);
    //return await this.userService.
  }

  @Put(':id')
  async update(
    @Param('id', new ParseIntPipe()) id,
    @Body() user: UpdateUserDTO,
  ): Promise<any> {
    return await this.userService.update(id, user);
    //return await this.userService.
  }

  @Post('signup')
  async signUp(@Body() user: CreateUserDTO): Promise<any> {
    return await this.userService.signup(user);
  }

  @Post('login')
  async login(@Body() user: CreateUserDTO): Promise<any> {
    return await this.userService.login(user);
  }
}
