import { Body, Get, Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { PhotosEntity } from 'src/photos/photo.entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { PasswordHasherService } from './auth/password-hasher.service';
import { CreateUserDTO } from './dto/create-users.dto';
import { UpdateUserDTO } from './dto/update-users.dto';
import { UserEntity } from './entities/users.entity';
import { User } from './interfaces/user';

@Injectable()
export class UsersService {
  hasherService: any;
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,

    @InjectRepository(PhotosEntity)
    private readonly photoRepository: Repository<PhotosEntity>,

    private passwordHasherService: PasswordHasherService,

    private jwtService: JwtService,
  ) {}

  async create(@Body() user: CreateUserDTO): Promise<User> {
    //or if -> cascade: ['insert', 'update']
    //@OneToMany(() => PhotosEntity, (photo) => photo.user, {
    // cascade: ['insert', 'update'],
    return await this.userRepository.save(user);

    let savedPhotos = [];
    if (Array.isArray(user.photos) && user.photos.length) {
      savedPhotos = await this.photoRepository.save(user.photos);
    }

    const savedUser = new UserEntity();
    savedUser.email = user.email;
    savedUser.password = user.password;
    savedUser.photos = savedPhotos;

    await this.userRepository.save(savedUser);
    return { ...savedUser, photos: savedPhotos };
  }

  async findAll(): Promise<User[]> {
    return this.userRepository.find({ relations: ['photos'] });
  }

  async findOne(id: number): Promise<User> {
    return this.userRepository.findOne(id, { relations: ['photos'] });
  }

  async delete(id: number): Promise<DeleteResult> {
    return this.userRepository.delete(id);
  }

  async update(id: number, user: UpdateUserDTO): Promise<UpdateResult> {
    return await this.userRepository.update(id, user);
  }

  async signup(doc: CreateUserDTO): Promise<any> {
    const user = await this.userRepository.findOne({ email: doc.email });
    if (user) {
      throw new UnauthorizedException(
        `User already created with this ${doc.email}`,
      );
    }

    const encPass = await this.passwordHasherService.hashPassword(doc.password);
    const newUser = { email: doc.email, password: encPass };
    return await this.userRepository.save(newUser);
  }

  async login(doc: CreateUserDTO): Promise<any> {
    const user = await this.userRepository.findOne({ email: doc.email });
    if (!user) {
      throw new UnauthorizedException(
        `Could not find any user with this ${doc.email}`,
      );
    }

    const comp = await this.passwordHasherService.comparePassword(
      doc.password,
      user.password,
    );
    if (comp == false) {
      throw new UnauthorizedException('Invalid password');
    }

    const token = await this.jwtService.signAsync(
      {
        email: user.email,
        id: user.id,
      },
      {
        expiresIn: '1111h',
      },
    );

    return { token };
  }

  async validateUserById(userId: string): Promise<any> {
    const user = await this.userRepository.findOne(userId);

    if (user) {
      return true;
    } else {
      return false;
    }
  }
}
