import { Strategy } from "passport-jwt";
import { UsersService } from "../../users.service";
declare const JwtStrategyService_base: new (...args: any[]) => Strategy;
export declare class JwtStrategyService extends JwtStrategyService_base {
    private userService;
    constructor(userService: UsersService);
    validate(payload: any): Promise<any>;
}
export {};
