export declare class PasswordHasherService {
    hashPassword(password: string): Promise<string>;
    comparePassword(text: any, pass: any): Promise<boolean>;
}
