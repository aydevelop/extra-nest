import {
  Controller,
  Post,
  Body,
  Get,
  Request,
  UseGuards,
} from "@nestjs/common";
import { User, SignupRsp } from "./interfaces/user";
import { UsersService } from "./users.service";
import { CreateUserDTO } from "./dto/create-user.dto";
import { AuthGuard } from "@nestjs/passport";

@Controller("users")
export class UsersController {
  constructor(private userService: UsersService) {}
  @Post("signup")
  async signUp(@Body() user: CreateUserDTO): Promise<SignupRsp> {
    return await this.userService.signup(user);
  }

  @Post("login")
  async login(@Body() user: CreateUserDTO): Promise<any> {
    return await this.userService.login(user);
  }

  @UseGuards(AuthGuard("jwt"))
  @Get("profile")
  async profile(@Request() req) {
    return req.user;
  }
}
