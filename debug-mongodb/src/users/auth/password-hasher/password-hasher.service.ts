import { Injectable } from "@nestjs/common";
import * as bcrypt from "bcrypt";

@Injectable()
export class PasswordHasherService {
  async hashPassword(password: string) {
    return await bcrypt.hash(password, 10);
  }

  async comparePassword(text, pass): Promise<boolean> {
    const res = await bcrypt.compare(text, pass);
    console.log("res: " + res);
    return res;
  }
}
