import { ProductService } from './../../core/services/product.service';
import { Component, OnInit } from '@angular/core';
import { Product } from 'core/models/product.model';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

import * as _ from 'lodash';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent implements OnInit {
  products: Product[] = [];

  constructor(private productService: ProductService, private router: Router) {}

  ngOnInit(): void {
    this.productService.findAll().subscribe((res) => {
      this.products = res.data;
    });
  }

  editProduct(productId: number): void {
    //alert(`test.......... ${productId}`);
    this.router.navigate(['/products', 'edit', productId]);
    //this.router.navigateByUrl('/products/');
  }

  deleteProduct(productId: number): void {
    this.productService.delete(productId).subscribe((res) => {
      const removedItems = _.remove(this.products, (item) => {
        return item.id === productId;
      });
    });
  }
}
