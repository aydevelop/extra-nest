import { ProductService } from './../../core/services/product.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'core/models/product.model';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss'],
})
export class ProductFormComponent implements OnInit {
  productForm: FormGroup = this.fb.group({
    name: ['', Validators.required],
    qty: [null, Validators.required],
    price: [null, Validators.required],
    partNumber: [''],
    dimension: [''],
    weight: [''],
    manufacturer: [''],
    origin: [''],
  });
  errors: any = null;
  product!: Product;

  constructor(
    private fb: FormBuilder,
    private ProductService: ProductService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.setProductToForm();
  }

  setProductToForm(): void {
    this.route.params.subscribe((params) => {
      const id = params.id;
      if (!id) {
        return;
      }

      this.ProductService.findOne(+id).subscribe(({ data }) => {
        this.product = data;
        this.productForm.patchValue({
          price: data.price,
          qty: data.qty,
          name: data.name,
        });
      });
    });
  }

  submitForm() {
    if (this.product) {
      this.ProductService.update(
        this.product.id,
        this.productForm.value
      ).subscribe((data) => {
        this.router.navigateByUrl('/products');
      });
      return;
    }

    this.ProductService.save(this.productForm.value).subscribe(
      (data) => {
        this.router.navigateByUrl('/products');
      },
      (err) => (this.errors = err.message)
    );
  }

  resetForm(): void {
    this.productForm.reset();
  }
}
