import { Component, OnInit } from '@angular/core';
import { ProfileService } from 'core/services/profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  userEmail = '';

  constructor(private profileService: ProfileService) {}

  ngOnInit(): void {
    this.profileService.getProfile().subscribe(({ data }) => {
      this.userEmail = data.email;
    });
  }
}
