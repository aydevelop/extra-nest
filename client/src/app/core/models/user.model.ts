export interface User {
  id: string;
  email: string;
  password: string;
}

export interface LoginRsp {
  token: string;
}

export interface SingupRsp {
  email: string;
}
