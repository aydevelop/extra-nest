export interface Product {
  id: number;
  name: string;
  qty: number;
  price: number;
  productDetails?: ProductDetailsEntity;
}

export interface ProductDetailsEntity {
  id?: string;
  partNumber?: string;
  dimension?: string;
  weight?: number;
  manufacturer?: string;
  origin?: string;
}

export interface UpdateProduct {
  name: string;
  qty: number;
  price: number;
}
