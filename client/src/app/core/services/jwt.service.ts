import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class JwtService {
  constructor() {}

  setToken(token: string): void {
    window.localStorage.setItem('auth_token', token);
  }

  getToken(): any {
    return window.localStorage.getItem('auth_token');
  }

  destroyToken(): void {
    window.localStorage.removeItem('auth_token');
  }
}
