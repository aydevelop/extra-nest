import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  constructor(private apiService: ApiService) {}

  //getProfile(): Observable<{ email: string; userId: string }> {
  getProfile(): Observable<any> {
    return this.apiService.get('/users/profile');
  }
}
