import { Injectable } from '@angular/core';
import { Product } from 'core/models/product.model';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private apiService: ApiService) {}

  findAll(): Observable<any> {
    return this.apiService.get('/products');
  }

  save(product: Product): Observable<any> {
    return this.apiService.post('/products', product);
  }

  update(id: number, product: Product): Observable<any> {
    console.log('id: ' + id);
    console.log(product);

    return this.apiService.put(`/products/${id}`, product);
  }

  findOne(productId: number): Observable<any> {
    return this.apiService.get(`/products/${productId}`);
  }

  delete(productId: number): Observable<any> {
    return this.apiService.delete(`/products/${productId}`);
  }
}
