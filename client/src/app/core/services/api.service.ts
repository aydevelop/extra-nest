import { HttpClient, HttpParams } from '@angular/common/http';
import { stringify } from '@angular/compiler/src/util';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { environment } from '../../../environments/environment';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  private formatErrors(error: any): any {
    return throwError(error.error);
  }

  get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    const url = `${environment.apiUrl}${path}`;
    return this.http.get(url, { params }).pipe(catchError(this.formatErrors));
  }

  post(path: string, body: object = {}): Observable<any> {
    const url = `${environment.apiUrl}${path}`;
    return this.http.post(url, body).pipe(catchError(this.formatErrors));
  }

  put(path: string, body: object = {}): Observable<any> {
    const url = `${environment.apiUrl}${path}`;
    return this.http.put(url, body).pipe(catchError(this.formatErrors));
  }

  delete(path: string): Observable<any> {
    const url = `${environment.apiUrl}${path}`;
    return this.http.delete(url).pipe(catchError(this.formatErrors));
  }
}
