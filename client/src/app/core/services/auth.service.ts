import { Injectable } from '@angular/core';
import { LoginRsp, SingupRsp, User } from 'core/models/user.model';
import { Observable, Subject } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private authChnnel = new Subject<boolean>();

  constructor(private apiService: ApiService) {}

  login(user: User): Observable<any> {
    this.authChnnel.next(true);
    return this.apiService.post('/users/login', user);
  }

  singup(user: User): Observable<SingupRsp> {
    return this.apiService.post('/users/signup', user);
  }

  logout(): void {
    this.authChnnel.next(false);
  }

  getAuthChannelAsObs(): Observable<boolean> {
    return this.authChnnel.asObservable();
  }
}
