import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'core/services/auth.service';
import { JwtService } from 'core/services/jwt.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  isAuth = false;
  authChannelSub!: Subscription;

  constructor(
    private jwtService: JwtService,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    if (this.jwtService.getToken()) {
      this.isAuth = true;
    }

    this.authChannelSub = this.authService
      .getAuthChannelAsObs()
      .subscribe((auth: boolean) => (this.isAuth = auth));
  }

  ngOnDestroy(): void {
    this.authChannelSub.unsubscribe();
  }

  logout(): void {
    this.jwtService.destroyToken();
    this.router.navigateByUrl('/login');
    this.isAuth = false;
    this.authService.logout();
  }
}
