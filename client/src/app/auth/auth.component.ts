import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'core/services/auth.service';
import { JwtService } from 'core/services/jwt.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private jwtService: JwtService
  ) {}

  title: string = 'Login';
  authForm: FormGroup = this.fb.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
  });

  ngOnInit(): void {
    this.title = this.router.url == '/login' ? 'Login' : 'Signup';
  }

  submitForm(): void {
    if (this.title == 'Login') {
      this.authService.login(this.authForm.value).subscribe(({ data }) => {
        console.log('token... ', data.token);
        this.jwtService.setToken(data.token);
        this.router.navigateByUrl('/products');
      });
    } else {
      this.authService.singup(this.authForm.value).subscribe((data) => {
        this.router.navigateByUrl('/products');
      });
    }
  }
}
